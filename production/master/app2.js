var app = angular.module('app', []);

app.controller('appController',  ['$scope','$http', function($scope, $http){

 $scope.getRequest = function () {
         $http.get("master/apiRest2.php?option=assets")
            .then(function successCallback(response){
                $scope.idList = response.data.resources;
                  console.log( response.data.resources);  
            }, function errorCallback(response){
                console.log("Unable to perform get request 1");
            });
    }

$scope.getRequestById = function (id) {
     console.log("el id es "+ id);  
         $http.get("master/apiRest2.php?option=by_id&id="+id)
            .then(function successCallback(response){
                $scope.vulnerabilitiesList = response.data.resources;
                $scope.idValue = id;
                  console.log('la data es '+ response.data.resources);  
            }, function errorCallback(response){
                console.log("Unable to perform get request 2");
            });
    }


}]); 
