var app = angular.module('app', []);

app.controller('appController',  ['$scope','$http', function($scope, $http){

 $scope.getRequest = function () {
         $http.get("master/apiRest.php?option=images")
            .then(function successCallback(response){
                $scope.imageList = response.data;
                  console.log( response.data);  
            }, function errorCallback(response){
                console.log("Unable to perform get request 1");
            });
    }

$scope.getRequestById = function (id) {
     console.log("el id es "+ id);  
         $http.get("master/apiRest.php?option=by_id&id="+id)
            .then(function successCallback(response){
                $scope.vulnerabilitiesList = response.data.vulnerabilities;
                $scope.imageDigest = response.data.imageDigest;
                  console.log('la data es '+ response.data.imageDigest);  
            }, function errorCallback(response){
                console.log("Unable to perform get request 2");
            });
    }


}]); 
